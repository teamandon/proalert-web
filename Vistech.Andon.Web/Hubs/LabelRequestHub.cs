﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Hubs
{
    [HubName("labelRequest")]
    public class LabelRequestHub : Hub
    {
        private static readonly ConnectionMapping<string> Connections = new ConnectionMapping<string>();
        private readonly ILabelRequest _labelRequest;

        public LabelRequestHub(ILabelRequest labelRequest)
        {
            _labelRequest = labelRequest;
        }

        public async Task SendShippingLabelRequest(ShippingLabelRequest slr)
        {
            await _labelRequest.SendShippingLabelRequest(slr);
        }

        public async Task SendScrapLabelRequest(int scrapId)
        {
            await _labelRequest.SendScrapLabelRequest(scrapId);
        }

        public override Task OnConnected()
        {
            var name = Context.User.Identity.Name;

            Connections.Add(name, Context.ConnectionId);
            _labelRequest.Connections = Connections;

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var name = Context.User.Identity.Name;

            Connections.Remove(name, Context.ConnectionId);
            _labelRequest.Connections = Connections;
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            var name = Context.User.Identity.Name;

            if (Connections.GetConnections(name).Contains(Context.ConnectionId)) return base.OnReconnected();
            Connections.Add(name, Context.ConnectionId);
            _labelRequest.Connections = Connections;

            return base.OnReconnected();
        }
    }
}