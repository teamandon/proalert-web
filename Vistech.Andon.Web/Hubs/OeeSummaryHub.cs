﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace Vistech.Andon.Web.Hubs
{
    [HubName("oeeSummary")]
    public class OeeSummaryHub : Hub
    {
        private IOeeSummary _iOeeSummary;
        public OeeSummaryHub(IOeeSummary iOeeSummary)
        {
            _iOeeSummary = iOeeSummary;
        }
        
        public async Task InitiateUpdate(int id)
        {
            await _iOeeSummary.InitiateUpdate(id);
        }

        public async Task UpdateAll()
        {
            await _iOeeSummary.UpdateAll();
        }
    }
}