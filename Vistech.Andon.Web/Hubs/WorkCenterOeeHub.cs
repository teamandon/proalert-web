﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Hubs
{
    [HubName("workcenterOee")]
    public class WorkCenterOeeHub : Hub
    {
        private static readonly ConnectionMapping<string> Connections = new ConnectionMapping<string>();
        private readonly IWorkCenterOee _workCenterOee;

        public WorkCenterOeeHub(IWorkCenterOee workCenterOee)
        {
            _workCenterOee = workCenterOee;
        }
        // keep around the chat method for future use
        public void SendChatMessage(string who, string message)
        {
            var name = Context.User.Identity.Name;

            foreach (var connectionId in Connections.GetConnections(who))
            {
                Clients.Client(connectionId).addChatMessage(name + ": " + message);
            }
        }

        public async Task UpdateWcGui(int id)
        {
            await _workCenterOee.UpdateWcGui(id);
        }

        public override Task OnConnected()
        {
            var name = Context.User.Identity.Name;

            Connections.Add(name, Context.ConnectionId);
            _workCenterOee.Connections = Connections;

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var name = Context.User.Identity.Name;

            Connections.Remove(name, Context.ConnectionId);
            _workCenterOee.Connections = Connections;
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            var name = Context.User.Identity.Name;

            if (!Connections.GetConnections(name).Contains(Context.ConnectionId))
            {
                Connections.Add(name, Context.ConnectionId);
                _workCenterOee.Connections = Connections;
            }

            return base.OnReconnected();
        }
    }
}