﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Vistech.Andon.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new RequireSecureConnectionFilter());
        }
    }

    public class RequireSecureConnectionFilter : RequireHttpsAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            // Until we get an SSL cert and we are not hosting to WWW, just return.  When we are ready to host to the www, implement the code below.

            //if (filterContext == null)
            //{
            //    throw new ArgumentNullException(nameof(filterContext));
            //}
            //if (filterContext.HttpContext.Request.IsLocal)
            //{
            //    return;
            //}
            //base.OnAuthorization(filterContext);
        }
    }
    //public class RequireHttpsInProductionAttribute : RequireHttpsAttribute
    //{
    //    private bool IsProduction { get; }

    //    public RequireHttpsInProductionAttribute(IHostingEnvironment environment)
    //    {
    //        if (environment == null)
    //            throw new ArgumentNullException(nameof(environment));
    //        this.IsProduction = environment.IsProduction();
    //    }
    //    public override void OnAuthorization(AuthorizationContext filterContext)
    //    {
    //        if (this.IsProduction)
    //            base.OnAuthorization(filterContext);
    //    }
    //    protected override void HandleNonHttpsRequest(AuthorizationContext filterContext)
    //    {
    //        if (this.IsProduction)
    //            base.HandleNonHttpsRequest(filterContext);
    //    }
    //}
}
