﻿using Autofac;
using Vistech.Andon.Model.Models;

namespace Vistech.Andon.Web.Modules
{
    public class EFModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(AndonVistechContext)).As(typeof(IContext)).InstancePerLifetimeScope();
        }
    }
}