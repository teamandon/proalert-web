﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Tools
{
    public class tsHelp
    {
        public static ICollection<Timesegment> FillTimesegments(IRepository _repo, int id, DateTime start, DateTime stop)
        {
            var list = new List<Timesegment>();
            foreach (var ts in _repo.Get<Timesegment>(x => x.WCProductTimelineID == id && ((x.Start <= start && (x.Stop ?? stop) > start) || (x.Start > start && x.Start < stop))))
            {
                var nts = new Timesegment
                {
                    Id = ts.Id,
                    WCProductTimelineID = ts.WCProductTimelineID,
                    Start = ts.Start < start ? start : ts.Start,
                    Stop = ts.Stop == null ? stop : ts.Stop > stop ? stop : ts.Stop,
                    PlannedDt = ts.PlannedDt,
                    UnplannedDt = ts.UnplannedDt,
                    ProductChangeOver = ts.ProductChangeOver
                };
                var tsid = ts.Id;
                if (ts.Start >= start && (ts.Stop ?? stop) <= stop)  // if ts falls within the time frame, add all related records.
                {
                    foreach (
                        var p in
                            _repo.Get<PlannedDtLog>(x => x.TimesegmentID == tsid,
                                includeProperties: "PlannedDt, Employee, Product"))
                    {
                        nts.PlannedDtLogs.Add(p);
                    }
                    foreach (
                        var c in
                            _repo.Get<CallLog>(x => x.TimesegmentID == tsid,
                                includeProperties: "Call, Employee, Product, Issue, SubCategory"))
                    {
                        nts.CallLogs.Add(c);
                    }
                    foreach (
                        var s in //_repo.Get<Scrap>(x => x.TimesegmentID == tsid))
                            _repo.Get<Scrap>(x => x.TimesegmentID == tsid && !x.Allocated,
                                includeProperties: "ScrapType, Product, LastStage, RejectCode, Employee"))
                    {
                        nts.Scraps.Add(s);
                    }
                    nts.CycleSummary = _repo.GetById<CycleSummary>(ts.CycleSummaryID);

                }
                else
                {
                    foreach (
                        var p in
                            _repo.Get<PlannedDtLog>(
                                x =>
                                    x.TimesegmentID == tsid &&
                                    ((x.Start <= start && (x.Stop ?? stop) > start) ||
                                     (x.Start > start && x.Start < stop)), includeProperties: "PlannedDt, Employee, Product")
                        )
                    {
                        nts.PlannedDtLogs.Add(p);
                    }
                    foreach (
                        var c in
                            _repo.Get<CallLog>(
                                x =>
                                    x.TimesegmentID == tsid &&
                                    ((x.InitiateDt <= start &&
                                      (x.Call.DT ? (x.ResolveDt ?? stop) : (x.ResponseDt ?? stop)) > start) ||
                                     (x.InitiateDt > start && x.InitiateDt < stop)),
                                includeProperties: "Call, Employee, Issue, SubCategory, Product"))
                    {
                        nts.CallLogs.Add(c);
                    }
                    foreach (
                        var s in
                            _repo.Get<Scrap>(
                                x => x.TimesegmentID == tsid && !x.Allocated && (x.ScrapDT >= start && x.ScrapDT < stop),
                                includeProperties: "ScrapType, Product, LastStage, RejectCode, Employee"))
                    {
                        nts.Scraps.Add(s);
                    }
                    var numCycles =
                        _repo.Get<CycleHistory>(
                            x =>
                                x.WorkCenterID == ts.WcProductTimeline.WorkCenterID && x.UpdateDT >= nts.Start &&
                                x.UpdateDT <= nts.Stop).Sum(x => x.Count);
                    if (numCycles > 0)
                        nts.CycleSummary = new CycleSummary {Count = numCycles};
                }
                list.Add(nts);
            }
            return list;
        }
    }
}