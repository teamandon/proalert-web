﻿
using System;
using System.Collections.Generic;
using System.Linq;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;

namespace Vistech.Andon.Web.Tools
{
    /// <summary>
    /// This is designed to be a reporting service.  It will create temporary turncated timesegments to report/calculate OEE data based on dynamic timeframes.
    /// THEREFORE, this class has it's own DBCONTEXT to prevent the TEMPORARY data from being saved to the database.
    /// </summary>
    public class TlRpt
    {
        public List<ProductTimelineStatsVM> GetTls(List<WorkCenter> wcs, DateTime start, DateTime stop)
        {
            //var model = new OEEListVM();
            var model = new List<ProductTimelineStatsVM>();
            var yesterday = start;
            var now = stop;
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    foreach (var wc in wcs)
                    {
                        var id = wc.Id;

                        var list = new List<ProductTimelineStatsVM>();

                        var query = repo.Get<WCProductTimeline>(
                            x =>
                                x.WorkCenterID == id &&
                                ((x.Start <= yesterday && (x.Stop ?? now) > yesterday) ||
                                 (x.Start > yesterday && x.Start < now)),
                            includeProperties: "Timesegments, ProductWcStatsHistory, WorkCenter");

                        foreach (var dr in query.ToList())
                        {
                            // removed || x.Current -- from selection criteria above.  Pulling in time lines that had not been updated during time frame.
                            //dr.Timesegments.AddRange(tsHelp.FillTimesegments(dr.WCProductTimelineID, yesterday, now));
                            dr.Stop = dr.Stop ?? now;
                            var tlStart = dr.Start < yesterday ? yesterday : dr.Start;
                            var tlStop = dr.Current ? now : dr.Stop > now ? now : dr.Stop;
                            var ctl = new WCProductTimeline
                            {
                                Id = dr.Id,
                                WorkCenterID = dr.WorkCenterID,
                                ProductID = dr.ProductID,
                                Start = tlStart,
                                Stop = tlStop,
                                TotalDT = dr.TotalDT,
                                TotalPlannedDT = dr.TotalPlannedDT,
                                PlannedProduction = dr.PlannedProduction,
                                OperationTime = dr.OperationTime,
                                ProductWcStatsHistoryID = dr.ProductWcStatsHistoryID,
                                Current = dr.Current,
                                Availability = dr.Availability,
                                Performance = dr.Performance,
                                Quality = dr.Quality,
                                OEE = dr.OEE,
                                LastDTReason = dr.LastDTReason,
                                WCProduct = dr.WCProduct,
                                UnplannedDt = dr.UnplannedDt,
                                PlannedDt = dr.PlannedDt,
                                Mfg = dr.Mfg,
                                Matl = dr.Matl,
                                Rework = dr.Rework,
                                Other = dr.Other,
                                ActualPieces = dr.ActualPieces,
                                ActualCycles = dr.ActualCycles,
                                ProductWcStatsHistory = dr.ProductWcStatsHistory,
                                WorkCenter = dr.WorkCenter,
                                Timesegments = tsHelp.FillTimesegments(repo, dr.Id, tlStart, tlStop ?? now)
                            };

                            list.Add(new ProductTimelineStatsVM(ctl, repo, true)
                            {
                                Start = ctl.Start,
                                Stop = ctl.Stop ?? now,
                                Updating = dr.Start < yesterday || dr.Current || dr.Stop > now,
                                WorkCenterID = id,
                                CacheID = ctl.Id.ToString()
                            });
                        }

                        foreach (var tl in list)
                        {
                            // when we added the TL, we set updating to know if we needed to recalulate
                            //if (tl.Updating && tl.TL.ProductWcStatsHistory != null)
                            //{
                            //    tl.SetValuesTlRpt(tl.Start, tl.Stop);
                            //}
                            model.Add(tl);
                        }

                    }

                }
            }
            return model;
        }
        public ProductTimelineStatsVM GetTlByWc(int wcid, DateTime start, DateTime stop)
        {
            //var model = new OEEListVM();
            var model = new ProductTimelineStatsVM();
            var yesterday = start;
            var now = stop;

            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    foreach (
                        var dr in
                        repo.Get<WCProductTimeline>(
                            x =>
                                x.WorkCenterID == wcid && x.Current,
                            includeProperties: "Timesegments, ProductWcStatsHistory, WorkCenter").ToList())
                    {
                        dr.Stop = dr.Stop ?? now;
                        var tlStart = dr.Start < yesterday ? yesterday : dr.Start;
                        var tlStop = dr.Current ? now : dr.Stop > now ? now : dr.Stop;
                        var ctl = new WCProductTimeline
                        {
                            Id = dr.Id,
                            WorkCenterID = dr.WorkCenterID,
                            ProductID = dr.ProductID,
                            Start = tlStart,
                            Stop = tlStop,
                            TotalDT = dr.TotalDT,
                            TotalPlannedDT = dr.TotalPlannedDT,
                            PlannedProduction = dr.PlannedProduction,
                            OperationTime = dr.OperationTime,
                            ProductWcStatsHistoryID = dr.ProductWcStatsHistoryID,
                            Current = dr.Current,
                            Availability = dr.Availability,
                            Performance = dr.Performance,
                            Quality = dr.Quality,
                            OEE = dr.OEE,
                            LastDTReason = dr.LastDTReason,
                            WCProduct = dr.WCProduct,
                            UnplannedDt = dr.UnplannedDt,
                            PlannedDt = dr.PlannedDt,
                            Mfg = dr.Mfg,
                            Matl = dr.Matl,
                            Rework = dr.Rework,
                            Other = dr.Other,
                            ActualPieces = dr.ActualPieces,
                            ActualCycles = dr.ActualCycles,
                            ProductWcStatsHistory = dr.ProductWcStatsHistory,
                            WorkCenter = dr.WorkCenter,
                            Timesegments = tsHelp.FillTimesegments(repo, dr.Id, tlStart, tlStop ?? now)
                        };

                        model = new ProductTimelineStatsVM(ctl, repo, true)
                        {
                            Start = ctl.Start,
                            Stop = ctl.Stop ?? now,
                            Updating = dr.Start < yesterday || dr.Current || dr.Stop > now,
                            WorkCenterID = wcid,
                            CacheID = ctl.Id.ToString()
                        };
                    }
                    if (model.TL.ProductWcStatsHistory != null)
                    {
                        model.WorkCenterID = wcid;
                        model.SetValuesTlRpt(model.Start, model.Stop);
                    }

                }
            }
            //model.SetValues(model.Start, model.Stop);

            return model;
        }
        public ProductTimelineStatsVM GetTl(int tlid, DateTime start, DateTime stop)
        {
            //var model = new OEEListVM();
            var model = new ProductTimelineStatsVM();
            var yesterday = start;
            var now = stop;

            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    foreach (
                        var dr in
                        repo.Get<WCProductTimeline>(
                            x =>
                                x.Id == tlid,
                            includeProperties: "Timesegments, ProductWcStatsHistory, WorkCenter").ToList())
                    {
                        dr.Stop = dr.Stop ?? now;
                        var tlStart = dr.Start < yesterday ? yesterday : dr.Start;
                        var tlStop = dr.Current ? now : dr.Stop > now ? now : dr.Stop;
                        var ctl = new WCProductTimeline
                        {
                            Id = dr.Id,
                            WorkCenterID = dr.WorkCenterID,
                            ProductID = dr.ProductID,
                            Start = tlStart,
                            Stop = tlStop,
                            TotalDT = dr.TotalDT,
                            TotalPlannedDT = dr.TotalPlannedDT,
                            PlannedProduction = dr.PlannedProduction,
                            OperationTime = dr.OperationTime,
                            ProductWcStatsHistoryID = dr.ProductWcStatsHistoryID,
                            Current = dr.Current,
                            Availability = dr.Availability,
                            Performance = dr.Performance,
                            Quality = dr.Quality,
                            OEE = dr.OEE,
                            LastDTReason = dr.LastDTReason,
                            WCProduct = dr.WCProduct,
                            UnplannedDt = dr.UnplannedDt,
                            PlannedDt = dr.PlannedDt,
                            Mfg = dr.Mfg,
                            Matl = dr.Matl,
                            Rework = dr.Rework,
                            Other = dr.Other,
                            ActualPieces = dr.ActualPieces,
                            ActualCycles = dr.ActualCycles,
                            ProductWcStatsHistory = dr.ProductWcStatsHistory,
                            WorkCenter = dr.WorkCenter,
                            Timesegments = tsHelp.FillTimesegments(repo, dr.Id, tlStart, tlStop ?? now)
                        };

                        model = new ProductTimelineStatsVM(ctl, repo, true)
                        {
                            Start = ctl.Start,
                            Stop = ctl.Stop ?? now,
                            Updating = dr.Start < yesterday || dr.Current || dr.Stop > now,
                            WorkCenterID = ctl.WorkCenterID ?? 0,
                            CacheID = ctl.Id.ToString()
                        };
                    }
                    if (model.Updating && model.TL.ProductWcStatsHistory != null)
                    {
                        model.SetValuesTlRpt(model.Start, model.Stop);
                    }
                }
            }
            return model;
        }
    }
}