﻿using Newtonsoft.Json;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;

namespace Vistech.Andon.Web.Tools
{
    /// <summary>
    /// No longer really a cache.  We are serializing the ProductTimelineVM and storing in the database for later recovery.
    /// not sure this gains us a damn thing.
    /// 
    /// Might be pulling this out soon.
    /// </summary>
    public class OeeCache
    {

        public ProductTimelineStatsVM GetByTlId(int id)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    return JsonConvert.DeserializeObject<ProductTimelineStatsVM>(repo.GetDdVmJson(id).Json);
                }
            }
        }

        public void CacheSummaryTL(ProductTimelineStatsVM oeeHeader)
        {
            using (var context = new ProAlertContext())
            {
                using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                {
                    var row = repo.GetDdVmJson(oeeHeader.TL.Id);
                    if (row != null)
                    {
                        row.Json = JsonConvert.SerializeObject(oeeHeader);
                        repo.UpdateDdVmJson(row);
                    }
                    else
                    {
                        repo.InsertDdVmJson(new DdVmJson
                        {
                            TlId = oeeHeader.TL.Id,
                            Json = JsonConvert.SerializeObject(oeeHeader)
                        });
                    }
                    repo.Save();
                }
            }
        }
    }
}