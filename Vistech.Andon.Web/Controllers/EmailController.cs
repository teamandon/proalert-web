﻿using System.Linq;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class EmailController : Controller
    {
        private IRepository _repo;

        public EmailController(IRepository repo)
        {
            _repo = repo;
        }
        //
        // GET: /Email/

        public ActionResult Index()
        {
            var emails = _repo.GetAll<Email>(includeProperties: "WorkCenter");
            return View(emails.ToList());
        }

        //
        // GET: /Email/Details/5

        public ActionResult Details(int id = 0)
        {
            var email = _repo.GetOne<Email>(x => x.Id == id, includeProperties:"WorkCenter");
            if (email == null)
            {
                return HttpNotFound();
            }
            return View(email);
        }

        //
        // GET: /Email/Create

        public ActionResult Create()
        {
            PopulateWorkCentersDD();
            return View();
        }

        //
        // POST: /Email/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Email email)
        {
            if (ModelState.IsValid)
            {
                _repo.Create(email, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }

            PopulateWorkCentersDD(email.WorkCenterID);  
            return View(email);
        }


        private void PopulateWorkCentersDD(object selectedWorkCenter = null)
        {
            var workcenterQuery = from wc in _repo.GetAll<WorkCenter>()
                                  orderby wc.Name
                                  select wc;
            ViewBag.WorkCenterID = new SelectList(workcenterQuery, "Id", "Name", selectedWorkCenter);
        }
        //
        // GET: /Email/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var email = _repo.GetById<Email>(id);
            if (email == null)
            {
                return HttpNotFound();
            }
            PopulateWorkCentersDD(email.WorkCenterID);
            return View(email);
        }

        //
        // POST: /Email/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Email email)
        {
            if (ModelState.IsValid)
            {
                var rowToUpdate = _repo.GetById<Email>(email.Id);
                rowToUpdate.SetupExceeded = email.SetupExceeded;
                rowToUpdate.Availablilty = email.Availablilty;
                rowToUpdate.Delay = email.Delay;
                rowToUpdate.Down = email.Down;
                rowToUpdate.Interval = email.Interval;
                rowToUpdate.OEE = email.OEE;
                rowToUpdate.Performance = email.Performance;
                rowToUpdate.Recipient = email.Recipient;
                rowToUpdate.RecipientEmail = email.RecipientEmail;
                rowToUpdate.Quality = email.Quality;
                rowToUpdate.Scrap = email.Scrap;
                _repo.Create(rowToUpdate, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            PopulateWorkCentersDD(email.WorkCenterID);
            return View(email);
        }

        //
        // GET: /Email/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var email = _repo.GetOne<Email>(x => x.Id == id, includeProperties: "WorkCenter");
            if (email == null)
            {
                return HttpNotFound();
            }
            return View(email);
        }

        //
        // POST: /Email/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _repo.Delete<Email>(id);
            _repo.Save();
            return RedirectToAction("Index");
        }

    }
}