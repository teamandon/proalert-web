﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Vistech.Andon.Web.Models;

namespace Vistech.Andon.Web.Controllers
{
    public class usersController : Controller
    {
        // GET: users
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                ViewBag.Name = user.Name;

                ViewBag.displayMenu = "No";

                if (isAdminUser())
                {
                    ViewBag.displayMenu = "Yes";
                }
                return View();
            }
            ViewBag.Name = "Not Logged IN";

            return View();
        }
        public bool isAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                var context = new ApplicationDbContext();
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var s = userManager.GetRoles(user.GetUserId());
                if (s[0].ToString() == "Admin")
                {
                    return true;
                }
                return false;

            }
            return false;
        }

        public async Task<IdentityResult> ChangePassword(string user, string password)
        {
            var context = new ApplicationDbContext();
            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);
            var cUser = store.Users.FirstOrDefault(x => x.UserName == user);
            if (cUser == null)
                return IdentityResult.Failed();
            var hashedNewPassword = userManager.PasswordHasher.HashPassword(password);
            await store.SetPasswordHashAsync(cUser, hashedNewPassword);
            await store.UpdateAsync(cUser);
            return IdentityResult.Success;
        }

    }
}