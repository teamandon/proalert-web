﻿using System;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using Vistech.Andon.Web.Models;

namespace Vistech.Andon.Web.Controllers
{
    public class WCListController : Controller
    {
        //
        // GET: /WCList/
        private IRepository _repo;

        public WCListController(IRepository repo)
        {
            _repo = repo;
        }
        public ActionResult WCList()
        {
            //db.Configuration.ProxyCreationEnabled = false;
            var query = from wc in _repo.GetAll<WorkCenter>()
                select wc;
            //db.Configuration.ProxyCreationEnabled = true;
            //return PartialView("_WCList", query);
            var context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            foreach (var wc in query)
            {
                if (roleManager.RoleExists("Operator"))
                {
                    if (!userManager.Users.Any(x => x.UserName == "WorkCenter" + wc.Id))
                    {
                        var user = new ApplicationUser
                        {
                            UserName = "WorkCenter" + wc.Id,
                            Email = "WorkCenter" + wc.Id + "@gmail.com"
                        };

                        var userPwd = "Aviator!" + wc.Id;

                        var chkUser = userManager.Create(user, userPwd);

                        //Add default User to Role Admin   
                        if (chkUser.Succeeded)
                        {
                            var result1 = userManager.AddToRole(user.Id, "Operator");
                            var tzi = _repo.GetOne<Employee>(x => x.LogIn == "tzi");
                            _repo.Create(new Employee
                            {
                                FirstName = user.UserName,
                                HireDate = DateTime.UtcNow,
                                TimeZone = tzi.TimeZone,
                                LogIn = user.UserName
                            });
                            _repo.Save();
                        }
                    }
                }
            }
            return View(query);
        }

    }
}
