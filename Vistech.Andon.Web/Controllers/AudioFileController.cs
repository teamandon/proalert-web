﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles="Admin")]
    public class AudioFileController : Controller
    {
        #region < Members >
        protected readonly IRepository _repo;
        #endregion
        //private UnitOfWork db = new UnitOfWork();

        //
        // GET: /AudioFile/
        public AudioFileController(IRepository repo)
        {
            _repo = repo;
        }

        public ActionResult Index()
        {
            return View(_repo.GetAll<AudioFile>());
        }

        //
        // GET: /AudioFile/Details/5

        public ActionResult Details(int id = 0)
        {
            var audiofile = _repo.GetById<AudioFile>(id);
            return audiofile == null ? (ActionResult) HttpNotFound() : View(audiofile);
        }

        //
        // GET: /AudioFile/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /AudioFile/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, RepeatCnt")] AudioFile audiofile, HttpPostedFileBase uploadFile)
        {
            if (!ModelState.IsValid) return View(audiofile);
            if (uploadFile != null)
            {
                audiofile.Sound = new byte[uploadFile.ContentLength];
                uploadFile.InputStream.Read(audiofile.Sound, 0, uploadFile.ContentLength);
            }
            //var name = User.Identity.Name;
            //var user = _repo.Get<Employee>(x => x.LogIn.Trim() == name.Trim()).FirstOrDefault();
            _repo.Create(audiofile, User.Identity.Name);
            _repo.Save();
            return RedirectToAction("Index");
        }

        //
        // GET: /AudioFile/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var audiofile = _repo.GetById<AudioFile>(id);
            if (audiofile == null)
            {
                return HttpNotFound();
            }
            return View(audiofile);
        }

        //
        // POST: /AudioFile/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, RepeatCnt")] AudioFile audiofile, HttpPostedFileBase uploadFile)
        {
            var audioToUpdate = _repo.GetById<AudioFile>(audiofile.Id); 
            if (audioToUpdate == null) return HttpNotFound();

            if (!ModelState.IsValid) return View(audiofile);
            if (uploadFile != null)
            {
                audioToUpdate.Sound = new byte[uploadFile.ContentLength];
                uploadFile.InputStream.Read(audioToUpdate.Sound, 0, uploadFile.ContentLength);
            }
            audioToUpdate.RepeatCnt = audiofile.RepeatCnt;
                
            _repo.Update(audioToUpdate, User.Identity.Name);
            _repo.Save();
            return RedirectToAction("Index");
        }

        //
        // GET: /AudioFile/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var audiofile = _repo.GetById<AudioFile>(id);
            ViewBag.Msg = _repo.Get<AutoMessage>(x => x.AudioFileID == id).Any() || _repo.Get<Call>(x => x.AudioFileID == id).Any() ? "Cannot delete." : "Are you sure you want to delete?";
            return audiofile == null ? (ActionResult) HttpNotFound() : View(audiofile);
        }

        //
        // POST: /AudioFile/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<AudioFile>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (!ex.Message.StartsWith("Violation of UNIQUE KEY constraint")) return RedirectToAction("Index");
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                ModelState.AddModelError("Name", "You cannot delete related data.");
            }

            return RedirectToAction("Index");
        }

    }
}