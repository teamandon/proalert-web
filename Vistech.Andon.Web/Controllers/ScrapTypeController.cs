﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class ScrapTypeController : Controller
    {
        private IRepository _repo;

        public ScrapTypeController(IRepository repo)
        {
            _repo = repo;
        }
        //
        // GET: /ScrapType/
        
        public ActionResult Index()
        {
            return View(_repo.GetAll<ScrapType>());
        }

        //
        // GET: /ScrapType/Details/5

        public ActionResult Details(int id = 0)
        {
            var scraptype =_repo.GetById<ScrapType>(id);
            if (scraptype == null)
            {
                return HttpNotFound();
            }
            return View(scraptype);
        }

        //
        // GET: /ScrapType/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ScrapType/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name, Description, Default")] ScrapType scraptype)
        {
            if (_repo.Get<ScrapType>(x => x.Name.Equals(scraptype.Name)).Any())
                ModelState.AddModelError("Name", "Already exists");
            var isDefaultAlready = _repo.Get<ScrapType>(x => x.Default).ToList().Any();
            if (isDefaultAlready && scraptype.Default)
            {
                // modelstate is not valid.
                ModelState.AddModelError("Default", "Cannot have two defaults");
            }
            if (ModelState.IsValid)
            {
                _repo.Create(scraptype, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                return View("Create", scraptype);
            }
            return View("Create", scraptype);
        }

        //
        // GET: /ScrapType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var scraptype = _repo.GetById<ScrapType>(id);
            if (scraptype == null)
            {
                return HttpNotFound();
            }
            return View(scraptype);
        }

        //
        // POST: /ScrapType/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id, Name, Description, Default")] ScrapType scraptype)
        {
            var isDefaultAlready = _repo.Get<ScrapType>(x => x.Default && !x.Name.Equals(scraptype.Name)).ToList().Any();
            if (isDefaultAlready && scraptype.Default)
            {
                // model state is not valid.
                ModelState.AddModelError("Default", "Cannot have two defaults");
            }
            var ost = _repo.GetById<ScrapType>(scraptype.Id);

            if (ModelState.IsValid)
            {
                ost.Default = scraptype.Default;
                ost.Description = scraptype.Description;

                _repo.Update(ost, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            if (Request.IsAjaxRequest())
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;

                return View("Edit", scraptype);
            }
            return View("Edit", scraptype);
        }

        //
        // GET: /ScrapType/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var scraptype = _repo.GetById<ScrapType>(id);
            ViewBag.Msg = _repo.Get<Scrap>(x => x.ScrapTypeID == id).Any() ? "Cannot delete." : "Are you sure you want to delete?";
            if (scraptype == null)
            {
                return HttpNotFound();
            }
            return View(scraptype);
        }

        //
        // POST: /ScrapType/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                var scraptype = _repo.GetById<ScrapType>(id);

                if (scraptype.Default)
                {
                    var nextStype = _repo.GetFirst<ScrapType>(x => !x.Default);
                    if (nextStype != null)
                    {
                        nextStype.Default = true;
                        _repo.Update(nextStype, User.Identity.Name);
                    }
                }
                _repo.Delete<ScrapType>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("Name", "You cannot delete related data.");
                }
            }
            return RedirectToAction("Index");
        }
    }
}