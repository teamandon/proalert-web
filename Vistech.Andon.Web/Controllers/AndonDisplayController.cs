﻿using System;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.ViewModels;

namespace Vistech.Andon.Web.Controllers
{
    ////[InitializeSimpleMembership]
    [Authorize(Roles = "Operator, Admin, Tech, Maint, AndonDisplay, Super")]
    public class AndonDisplayController : Controller
    {
        private IRepository _repo;

        public AndonDisplayController(IRepository repo)
        {
            _repo = repo;
        }
        // GET: /MainDisplay/
        public ActionResult AndonDisplay()
        {
            var model = new AndonDisplayVM { IsForWc = false, Repo = _repo };
            ViewBag.ServerTime = DateTime.Now;

            return View(model.GetMessages);
        }

        public ActionResult UpdateDisplay()
        {
            var model = new AndonDisplayVM { IsForWc = false, Repo = _repo };
            ViewBag.ServerTime = DateTime.Now;
            return PartialView("_AndonDisplay", model.GetMessages);
        }

        public ActionResult IsAndonDisplay()
        {
            const bool bPlaySounds = true;
            ViewBag.ServerTime = DateTime.Now;
            return Json(new { play = bPlaySounds }, JsonRequestBehavior.AllowGet);
        }
    }
}
