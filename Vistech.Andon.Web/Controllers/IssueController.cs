﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class IssueController : Controller
    {
        private IRepository _repo;

        public IssueController(IRepository repo)
        {
            _repo = repo;
        }
        //
        // GET: /Issue/

        public ActionResult Index()
        {
            var issues = _repo.GetAll<Issue>(includeProperties: "SubCategory");
            return View(issues.ToList());
        }

        //
        // GET: /Issue/Details/5

        public ActionResult Details(int id = 0)
        {
            var issue = _repo.GetOne<Issue>(x => x.Id == id, includeProperties: "SubCategory");
            if (issue == null)
            {
                return HttpNotFound();
            }
            return View(issue);
        }

        //
        // GET: /Issue/Create

        public ActionResult Create()
        {
            var vm = new IssueVM();

            return View(vm);
        }
        //
        // POST: /Issue/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IssueVM vm)
        {
            if (_repo.Get<Issue>(x => x.Name.Equals(vm.Name)).Any())
                ModelState.AddModelError("Name", "Already exists");
            if (ModelState.IsValid)
            {
                var nI = new Issue {Name = vm.Name, Description = vm.Description};
                _repo.Create(nI, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }

            return View(vm);
        }

        //
        // GET: /Issue/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var issue = _repo.GetById<Issue>(id);
            if (issue == null)
            {
                return HttpNotFound();
            }
            var vm = new IssueVM { Id = issue.Id, Name = issue.Name, Description = issue.Description};
            return View(vm);
        }

        //
        // POST: /Issue/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(IssueVM vm)
        {
            if (ModelState.IsValid)
            {
                var rowToUpdate = _repo.GetById<Issue>(vm.Id);
                rowToUpdate.Description = vm.Description;

                try
                {
                    _repo.Update(rowToUpdate, User.Identity.Name);
                    _repo.Save();
                    return RedirectToAction("Index");
                }
                catch (DbEntityValidationException ex)
                {
                    _repo.InsertError(new AppError
                    {
                        ErrorDT = DateTime.UtcNow,
                        ErrorMsg = ex.Message,
                        Source = "Issue Controller : Edit"
                    });
                    _repo.Save();
                }
            }
            return View(vm);
        }

        //
        // GET: /Issue/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var issue = _repo.GetById<Issue>(id); //<Issue>(includeProperties:"SubCategory");
            ViewBag.Msg = _repo.Get<CallLog>(x => x.IssueID == id).Any() ? "Cannot delete." : "Are you sure you want to delete?";
            if (issue == null)
            {
                return HttpNotFound();
            }
 
            return View(issue);
        }

        //
        // POST: /Issue/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<Issue>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("Name", "You cannot delete related data.");
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult SubCategoryCreate()
        {
            if (Request != null && Request.IsAjaxRequest())
                return PartialView("_SubCategoryCreate", new SubCategory());

            return View("_SubCategoryCreate", new SubCategory());
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SubCategoryCreate(SubCategory model)
        {
            var statusCode = 0;
            var errorMsg = string.Empty;
            StringBuilder errorMessage = null;
            if (!ModelState.IsValid)
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_SubCategoryCreate", model);
                }
                return PartialView("_SubCategoryCreate", model);
            }
            try
            {
                _repo.Create(model, User.Identity.Name);
                _repo.Save();
            }
            catch (Exception exp)
            {
                errorMessage = new StringBuilder(200);
                errorMessage.AppendFormat(
                    "<div class=\"validation-summary-errors\" title\"Server Error\">{0}</div>",
                    exp.GetBaseException().Message);
                statusCode = (int)HttpStatusCode.InternalServerError; // = 500
            }

            if (Request.IsAjaxRequest())
            {
                if (statusCode > 0)
                {
                    Response.StatusCode = statusCode;
                    return Content(errorMessage.ToString());
                }
                TempData["message"] = "Subcategory was added.";
                var id = _repo.Get<SubCategory>().Last().Id;
                return Json(new { sl = new SelectList(_repo.GetAll<SubCategory>(), "Id", "Name"), idx = id }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Create");
        }
    }
}