﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;

namespace Vistech.Andon.Web.Controllers
{
    public class pDD
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class ProductWorkCenterStatController : Controller
    {
        private IRepository _repo;

        public ProductWorkCenterStatController(IRepository repo)
        {
            _repo = repo;
        }
        //
        // GET: /ProductWorkCenterStat/

        public ActionResult Index(int? workCenterId)
        {
            ViewBag.WorkCenterID = new SelectList(_repo.GetAll<WorkCenter>().OrderBy(x => x.Name), "Id", "Name");
            if (workCenterId == null)
            {
                //var productworkcenterstats = db.ProductWorkCenterStats.Include(p => p.WorkCenter).Include(p => p.Product).OrderBy(x => x.WorkCenter.Name).ThenBy(x => x.Product.Name);
                var productworkcenterstats =
                    _repo.GetAll<ProductWorkCenterStat>(
                        orderBy: o => o.OrderBy(x => x.WorkCenter.Name).ThenBy(x => x.Product.Name),
                        includeProperties: "WorkCenter, Product");
                return View(productworkcenterstats.ToList());
            }
            else
            {
                //var productworkcenterstats = db.ProductWorkCenterStats.Include(p => p.WorkCenter).Include(p => p.Product).Where(x=>x.WorkCenterID == WorkCenterID).OrderBy(x => x.WorkCenter.Name).ThenBy(x => x.Product.Name);
                var productworkcenterstats = _repo.Get<ProductWorkCenterStat>(p => p.WorkCenterID == workCenterId, o => o.OrderBy(x => x.WorkCenter.Name).ThenBy(x => x.Product.Name), "WorkCenter, Product");
                return View(productworkcenterstats.ToList());
               
            }
        }

        public ActionResult Details(int id = 0)
        {
            var productworkcenterstat = _repo.GetOne<ProductWorkCenterStat>(x=>x.Id == id, includeProperties: "WorkCenter, Product");
            if (productworkcenterstat == null)
            {
                return HttpNotFound();
            }
            return View(productworkcenterstat);
        }

        //
        // GET: /ProductWorkCenterStat/Create

        public ActionResult Create(int? wcid = null)
        {
            var model = new ProductWCStatsCreateVM
            {
                ProductWorkCenterStat = new ProductWorkCenterStat {WorkCenterID = wcid ?? -1},
                Products = PopulateProductsDD(wcid),
                WorkCenters = PopulateWorkCentersDD(wcid)
            };
            return View(model);
        }

        private SelectList PopulateWorkCentersDD(object selectedWorkCenter = null)
        {
            var workcenterQuery = from wc in _repo.GetAll<WorkCenter>()
                orderby wc.Name
                select wc;
            return new SelectList(workcenterQuery, "Id", "Name", selectedWorkCenter);
        }

        private SelectList PopulateProductsDD(int? wcid = null, object selectedProduct = null)
        {
            var productQuery = from p in _repo.GetAll<Product>()
                orderby p.Name
                select new { p.Id, p.Name };

            if (wcid != null)
            {
                var tempList = productQuery;
                var newlist = new List<pDD>();

                foreach (var dr in tempList)
                {
                    var pid = dr.Id;
                    var found =
                        _repo.Get<ProductWorkCenterStat>(x => x.WorkCenterID == wcid && x.ProductID == pid).Any();
                    if (!found || dr.Id == ((int?)selectedProduct ?? 0))
                        newlist.Add(new pDD
                        {
                            Id = dr.Id,
                            Name = dr.Name
                        });
                }
                return new SelectList(newlist, "Id", "Name", selectedProduct);
            }
            return new SelectList(productQuery, "Id", "Name", selectedProduct);
        }
        //
        // POST: /ProductWorkCenterStat/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductWCStatsCreateVM vm)
        {
            if (vm.ProductWorkCenterStat.UnitsPerCycle == 0)
                ModelState.AddModelError("UnitsPerCycle", "Cannot be zero");
            if (vm.ProductWorkCenterStat.CyclesPerHour == 0)
                ModelState.AddModelError("CyclesPerHour", "Cannot be zero");

            if (ModelState.IsValid)
            {
                _repo.Create(vm.ProductWorkCenterStat, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            vm.Products = PopulateProductsDD(vm.ProductWorkCenterStat.WorkCenterID, vm.ProductWorkCenterStat.ProductID);
            vm.WorkCenters = PopulateWorkCentersDD(vm.ProductWorkCenterStat.WorkCenterID);
            return View(vm);
        }

        //
        // GET: /ProductWorkCenterStat/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var productworkcenterstat = _repo.GetById<ProductWorkCenterStat>(id);
            if (productworkcenterstat == null)
            {
                return HttpNotFound();
            }
            var vm = new ProductWCStatsUpdateVM
            {
                ProductWorkCenterStat = productworkcenterstat,
                Products = PopulateProductsDD(productworkcenterstat.WorkCenterID, productworkcenterstat.ProductID),
                WorkCenters = PopulateWorkCentersDD(productworkcenterstat.WorkCenterID)
            };
            
            return View(vm);
        }

        //
        // POST: /ProductWorkCenterStat/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductWCStatsUpdateVM vm)
        {
            var statToUpdate = _repo.GetOne<ProductWorkCenterStat>(x => x.Id == vm.ProductWorkCenterStat.Id, includeProperties: "Product, WorkCenter");

            if (vm.ProductWorkCenterStat.UnitsPerCycle == 0)
                ModelState.AddModelError("UnitsPerCycle", "Cannot be zero");
            if (vm.ProductWorkCenterStat.CyclesPerHour == 0)
                ModelState.AddModelError("CyclesPerHour", "Cannot be zero");

            if (ModelState.IsValid)
            {
                statToUpdate.ProductID = vm.ProductWorkCenterStat.ProductID;
                statToUpdate.WorkCenterID = vm.ProductWorkCenterStat.WorkCenterID;
                statToUpdate.Yield = vm.ProductWorkCenterStat.Yield;
                statToUpdate.CyclesPerHour = vm.ProductWorkCenterStat.CyclesPerHour;
                statToUpdate.SetUpTime = vm.ProductWorkCenterStat.SetUpTime;
                statToUpdate.Available = vm.ProductWorkCenterStat.Available;
                statToUpdate.Performance = vm.ProductWorkCenterStat.Performance;
                statToUpdate.Quality = vm.ProductWorkCenterStat.Quality;
                statToUpdate.UnitsPerCycle = vm.ProductWorkCenterStat.UnitsPerCycle;
                statToUpdate.AvailableCap = vm.ProductWorkCenterStat.AvailableCap;
                statToUpdate.PerformanceCap = vm.ProductWorkCenterStat.PerformanceCap;
                statToUpdate.QualityCap = vm.ProductWorkCenterStat.QualityCap;

                _repo.Update(statToUpdate, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            vm.WorkCenters = PopulateWorkCentersDD(vm.ProductWorkCenterStat.WorkCenterID);
            vm.Products = PopulateProductsDD(vm.ProductWorkCenterStat.WorkCenterID, vm.ProductWorkCenterStat.ProductID);
            return View(vm);
        }

        //
        // GET: /ProductWorkCenterStat/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var productworkcenterstat = _repo.GetOne<ProductWorkCenterStat>(x=> x.Id == id, includeProperties:"WorkCenter, Product");
            if (productworkcenterstat == null)
            {
                return HttpNotFound();
            }
            return View(productworkcenterstat);
        }

        //
        // POST: /ProductWorkCenterStat/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<ProductWorkCenterStat>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("Name", "You cannot delete related data.");
                }
            }

            return RedirectToAction("Index");
        }
    }
}