﻿using System;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;
using timeconversions = Vistech.Andon.Web.Tools.timeconversions;

namespace Vistech.Andon.Web.Controllers
{
    public class ShiftStartTimesController : Controller
    {
        private IRepository _repo;

        public ShiftStartTimesController(IRepository repo)
        {
            _repo = repo;
        }
        // GET: ShiftStartTimes
        public ActionResult Index()
        {
            var l = _repo.GetAll<ShiftStartTimes>();
            return View(l);
        }

        // GET: ShiftStartTimes/Details/5
        public ActionResult Details(int id)
        {
            var s = _repo.GetById<ShiftStartTimes>(id);
            return View(s);
        }

        // GET: ShiftStartTimes/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: ShiftStartTimes/Create
        [HttpPost]
        public ActionResult Create(ShiftTimeVM model)
        {
            if (ModelState.IsValid)
            {
                var tzi = (TimeZoneInfo)Session["tzi"];

                try
                {
                    var shiftTime = new ShiftStartTimes
                    {
                        FirstStart = timeconversions.ClientToUTC(tzi, DateTime.Today.AddHours(5)),
                        SecondStart = timeconversions.ClientToUTC(tzi, DateTime.Today.AddHours(14)),
                        ThirdStart = timeconversions.ClientToUTC(tzi, DateTime.Today.AddHours(23))
                    };
                    _repo.Create(shiftTime, User.Identity.Name);
                    _repo.Save();
                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        // GET: ShiftStartTimes/Edit/5
        public ActionResult Edit(int id)
        {
            var s =_repo.GetById<ShiftStartTimes>(id);
            return View(s);
        }

        // POST: ShiftStartTimes/Edit/5
        [HttpPost]
        public ActionResult Edit(ShiftStartTimes model)
        {
            if (ModelState.IsValid)
            {
                var toUpdt = _repo.GetById<ShiftStartTimes>(model.Id);
                toUpdt.FirstStart = model.FirstStart;
                toUpdt.SecondStart = model.SecondStart;
                toUpdt.ThirdStart = model.ThirdStart;
                try
                {
                    // TODO: Add update logic here

                    return RedirectToAction("Index");
                }
                catch
                {
                    return View(model);
                }
            }
            return View(model);
        }

        // GET: ShiftStartTimes/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ShiftStartTimes/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
