﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class RejectCodeController : Controller
    {
        private IRepository _repo;

        public RejectCodeController(IRepository repo)
        {
            _repo = repo;
        }
        //
        // GET: /RejectCode/

        public ActionResult Index()
        {

            return View(_repo.GetAll<RejectCode>());
        }

        //
        // GET: /RejectCode/Details/5

        public ActionResult Details(int id = 0)
        {
            var rejectcode = _repo.GetById<RejectCode>(id);
            if (rejectcode == null)
            {
                return HttpNotFound();
            }
            return View(rejectcode);
        }

        //
        // GET: /RejectCode/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /RejectCode/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RejectCode rejectcode)
        {
            if (_repo.Get<RejectCode>(x => x.Name.Equals(rejectcode.Name)).Any())
                ModelState.AddModelError("Name", "Already Exists");
            if (ModelState.IsValid)
            {
                _repo.Create(rejectcode, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }

            return View(rejectcode);
        }

        //
        // GET: /RejectCode/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var rejectcode = _repo.GetById<RejectCode>(id);
            if (rejectcode == null)
            {
                return HttpNotFound();
            }
            return View(rejectcode);
        }

        //
        // POST: /RejectCode/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RejectCode rejectcode)
        {
            if (ModelState.IsValid)
            {
                var rcToUpdt = _repo.GetById<RejectCode>(rejectcode.Id);

                rcToUpdt.Description = rejectcode.Description;
                _repo.Update(rcToUpdt, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            return View(rejectcode);
        }

        //
        // GET: /RejectCode/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var rejectcode = _repo.GetById<RejectCode>(id);
            ViewBag.Msg = _repo.Get<Scrap>(x => x.RejectCodeID == id).Any() ? "Cannot delete." : "Are you sure you want to delete?";
            if (rejectcode == null)
            {
                return HttpNotFound();
            }
            return View(rejectcode);
        }

        //
        // POST: /RejectCode/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<RejectCode>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("Name", "You cannot delete related data.");
                }
            }
            return RedirectToAction("Index");
        }
    }
}