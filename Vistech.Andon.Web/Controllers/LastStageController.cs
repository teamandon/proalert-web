﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class LastStageController : Controller
    {
        private IRepository _repo;

        public LastStageController(IRepository repo)
        {
            _repo = repo;
        }

        //
        // GET: /LastStage/

        public ActionResult Index()
        {
            return View(_repo.GetAll<LastStage>());
        }

        //
        // GET: /LastStage/Details/5

        public ActionResult Details(int id = 0)
        {
            var laststage = _repo.GetById<LastStage>(id);
            if (laststage == null)
            {
                return HttpNotFound();
            }
            return View(laststage);
        }

        //
        // GET: /LastStage/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /LastStage/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LastStage laststage)
        {
            if (_repo.Get<LastStage>(x => x.Name.Equals(laststage.Name)).Any())
                ModelState.AddModelError("Name", "Already exists");
            if (ModelState.IsValid)
            {
                _repo.Create(laststage, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }

            return View(laststage);
        }

        //
        // GET: /LastStage/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var laststage = _repo.GetById<LastStage>(id);
            if (laststage == null)
            {
                return HttpNotFound();
            }
            return View(laststage);
        }

        //
        // POST: /LastStage/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LastStage laststage)
        {

            if (ModelState.IsValid)
            {
                var lsToUpdt = _repo.GetById<LastStage>(laststage.Id);
                lsToUpdt.Description = laststage.Description;

                _repo.Update(lsToUpdt, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            return View(laststage);
        }

        //
        // GET: /LastStage/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var laststage = _repo.GetById<LastStage>(id);
            ViewBag.Msg = _repo.Get<Scrap>(x => x.LastStageID == id).Any() ? "Cannot delete." : "Are you sure you want to delete?";

            if (laststage == null)
            {
                return HttpNotFound();
            }
            return View(laststage);
        }

        //
        // POST: /LastStage/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<LastStage>(id);
                _repo.Save();
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("Name", "You cannot delete related data.");
                }
            }

            return RedirectToAction("Index");
        }
    }
}