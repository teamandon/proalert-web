﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;
using timeconversions = ProAlert.Andon.Service.Common.timeconversions;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class EmployeeController : Controller
    {
        private IRepository _repo;

        public EmployeeController(IRepository repo)
        {
            _repo = repo;
        }
        //
        // GET: /Employee/

        public ActionResult Index()
        {
            //var employees = db.Employees.Include(e => e.Position);
            var employees = _repo.Get<Employee>(includeProperties: "Position");
            return View(employees.ToList());
        }

        //
        // GET: /Employee/Details/5

        public ActionResult Details(int id = 0)
        {
            Employee employee = _repo.GetById<Employee>(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        //
        // GET: /Employee/Create

        public ActionResult Create()
        {
            //PopulatePositionDD();
            //PopulateTimeZoneDD();
            var vm = new EmployeeVM
            {
                Positions = _repo.GetAll<Position>(orderBy: s => s.OrderBy(o => o.Title)),
                TimeZones = new SelectList(TimeZoneInfo.GetSystemTimeZones().ToList(), "ID", "DisplayName")
            };
            return View(vm);
        }

        private void PopulateTimeZoneDD(object selectedTimeZone = null)
        {
            ViewBag.TimeZone = new SelectList(TimeZoneInfo.GetSystemTimeZones().ToList(), "ID", "DisplayName", selectedTimeZone);
        }

        private void PopulatePositionDD(object selectedPosition = null)
        {
            var positionQuery = from p in _repo.GetAll<Position>() 
                orderby p.Title
                select p;
            ViewBag.PositionID = new SelectList(positionQuery, "Id", "Title", selectedPosition);
        }
        //
        // POST: /Employee/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EmployeeVM vm)
        {
            if (_repo.Get<Employee>(x => x.Pin == vm.Employee.Pin).Any())
                ModelState.AddModelError("Employee.Pin", "Pin already used. Choose another.");
            if (_repo.Get<Employee>(x => x.LogIn == vm.Employee.LogIn).Any())
                ModelState.AddModelError("Employee.LogIn", "Login already used. Choose another.");

            if (ModelState.IsValid)
            {
                //employee.HireDate = DateTime.Now;

                _repo.Create(vm.Employee, User.Identity.Name);
                try
                {
                    _repo.Save();
                }
                catch (SqlException ex)
                { 
                    _repo.InsertError(new AppError()
                    {
                        ErrorDT = DateTime.Now,
                        ErrorMsg = ex.Message.ToString(),
                        Source = "Employee Create",
                        UserID = 0
                    });
                    throw;
                }
                return RedirectToAction("Index");
            }
            vm.Positions = _repo.GetAll<Position>(orderBy: s => s.OrderBy(o => o.Title));
            vm.TimeZones = new SelectList(TimeZoneInfo.GetSystemTimeZones().ToList(), "ID", "DisplayName");
            return View(vm);
        }

        //
        // GET: /Employee/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var employee = _repo.GetById<Employee>(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            var vm = new EmployeeVM
            {
                Employee = employee,
                Positions = _repo.GetAll<Position>(orderBy: s => s.OrderBy(o => o.Title)),
                TimeZones = new SelectList(TimeZoneInfo.GetSystemTimeZones().ToList(), "ID", "DisplayName")
            };
            return View(vm);
        }

        //
        // POST: /Employee/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeVM vm)
        {
            var empToUpdate = _repo.GetById<Employee>(vm.Employee.Id);

            if (empToUpdate == null) return HttpNotFound();
            if (_repo.Get<Employee>(x => x.LogIn == vm.Employee.LogIn && x.Id != vm.Employee.Id).Any())
                ModelState.AddModelError("Employee.LogIn", "LogIn already exists. Choose another.");
            if (_repo.Get<Employee>(x => x.Pin == vm.Employee.Pin && x.Id != vm.Employee.Id).Any())
                ModelState.AddModelError("Employee.Pin", "Pin already used. Choose another.");

            if (ModelState.IsValid)
            {
                empToUpdate.LogIn = vm.Employee.LogIn;
                empToUpdate.PositionID = vm.Employee.PositionID;
                empToUpdate.HireDate = timeconversions.ClientToUTC(vm.Employee.GetTimeZoneInstance(), vm.Employee.HireDate);


                if (vm.Employee.TerminationDate != null)
                {
                    var d = vm.Employee.TerminationDate ?? DateTime.Now;
                    vm.Employee.TerminationDate = timeconversions.ClientToUTC(vm.Employee.GetTimeZoneInstance(), d);
                }
                empToUpdate.FirstName = vm.Employee.FirstName;
                empToUpdate.LastName = vm.Employee.LastName;
                empToUpdate.Email = vm.Employee.Email;
                empToUpdate.Cell = vm.Employee.Cell;
                empToUpdate.Pin = vm.Employee.Pin;
                empToUpdate.TimeZone = vm.Employee.TimeZone;

                _repo.Update(empToUpdate, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            vm.Positions = _repo.GetAll<Position>(orderBy: s => s.OrderBy(o => o.Title));
            vm.TimeZones = new SelectList(TimeZoneInfo.GetSystemTimeZones().ToList(), "ID", "DisplayName");


            return View(vm);
        }



        //
        // GET: /Employee/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var employee = _repo.GetById<Employee>(id);
            ViewBag.Msg = _repo.Get<Scrap>(x => x.EmployeeID == id).Any() || _repo.Get<CallLog>(x => x.EmployeeID == id).Any() ? "Cannot delete." : "Are you sure you want to delete?";
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        //
        // POST: /Employee/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _repo.Delete<Employee>(id);
                _repo.Save(); 
            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Violation of UNIQUE KEY constraint"))
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    ModelState.AddModelError("Name", "You cannot delete related data.");
                }
            } 
            return RedirectToAction("Index");
        }

        public ActionResult PositionCreate()
        {
            if (Request != null && Request.IsAjaxRequest())
                return PartialView("_PositionCreate", new Position());

            return View("_PositionCreate", new Position());
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PositionCreate(Position model)
        {
            var statusCode = 0;
            var errorMsg = string.Empty;
            StringBuilder errorMessage = null;
            if (model.Title == null)
                ModelState.AddModelError("","Position has to be filled");
            if (_repo.Get<Position>(x => x.Title.Equals(model.Title)).Any())
                ModelState.AddModelError("Title", "Already exists");
            if (!ModelState.IsValid)
            {
                if (Request.IsAjaxRequest())
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return PartialView("_PositionCreate", model);
                }
                return PartialView("_PositionCreate", model);
            }
            try
            {
                _repo.Create(model, User.Identity.Name);
                _repo.Save();
            }
            catch (Exception exp)
            {
                errorMessage = new StringBuilder(200);
                errorMessage.AppendFormat(
                    "<div class=\"validation-summary-errors\" title\"Server Error\">{0}</div>",
                    exp.GetBaseException().Message);
                statusCode = (int)HttpStatusCode.InternalServerError; // = 500
            }
            if (Request.IsAjaxRequest())
            {
                if (statusCode > 0)
                {
                    Response.StatusCode = statusCode;
                    return Content(errorMessage.ToString());
                }
                TempData["message"] = "Position was added.";
                var i = _repo.GetFirst<Position>(x => x.Title.Equals(model.Title)).Id;
                return Json(new {sl = new SelectList(_repo.GetAll<Position>(orderBy: s => s.OrderBy(o => o.Title)), "Id", "Title", model.Title), idx = i }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index");
        }

    }
}