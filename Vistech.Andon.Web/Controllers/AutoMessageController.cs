﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using timeconversions = ProAlert.Andon.Service.Common.timeconversions;

namespace Vistech.Andon.Web.Controllers
{
    //[InitializeSimpleMembership]
    [Authorize(Roles = "Admin")]
    public class AutoMessageController : Controller
    {
        #region < Members >
        private readonly IRepository _repo;
        #endregion

        public AutoMessageController(IRepository repo)
        {
            _repo = repo;
        }
        //
        // GET: /AutoMessage/

        public ActionResult Index()
        {
            var automessages = _repo.Get<AutoMessage>(includeProperties: "AudioFile, Employee");
            return View(automessages.ToList());
        }

        //
        // GET: /AutoMessage/Details/5

        public ActionResult Details(int id = 0)
        {
            var automessage = _repo.GetOne<AutoMessage>(x => x.Id == id, includeProperties:"AudioFile, Employee");
            if (automessage == null)
            {
                return HttpNotFound();
            }
            return View(automessage);
        }

        //
        // GET: /AutoMessage/Create

        public ActionResult Create()
        {
            PopulateAudioFileDD();
            return View();
        }

        private void PopulateAudioFileDD(object selectedAudioFile = null)
        {
            var audiofileQuery = from af in _repo.Get<AudioFile>()
                                 orderby af.Name
                                 select new { af.Id, af.Name };

            ViewBag.AudioFileID = new SelectList(audiofileQuery, "Id", "Name", selectedAudioFile);
        }
        //
        // POST: /AutoMessage/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AutoMessage automessage, HttpPostedFileBase uploadFile)
        {

            if (ModelState.IsValid)
            {
                if (uploadFile != null)
                {
                    automessage.Image = new byte[uploadFile.ContentLength];
                    uploadFile.InputStream.Read(automessage.Image, 0, uploadFile.ContentLength);
                }
                var tzi = (TimeZoneInfo)Session["tzi"];
                var name = User.Identity.Name;
                var user = _repo.GetFirst<Employee>(x => x.LogIn.Trim() == name.Trim());
                if (user != null)
                    automessage.EmployeeID = user.Id;
                automessage.Start = timeconversions.ClientToUTC(tzi, automessage.Start);
                _repo.Create(automessage, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            PopulateAudioFileDD(automessage.AudioFileID);
            return View(automessage);
        }

        //
        // GET: /AutoMessage/Edit/5

        public ActionResult Edit(int id = 0)
        {
            var automessage = _repo.GetOne<AutoMessage>(x => x.Id == id, includeProperties: "AudioFile, Employee");
            if (automessage == null)
            {
                return HttpNotFound();
            }
            PopulateAudioFileDD(automessage.AudioFileID);
            return View(automessage);
        }

        //
        // POST: /AutoMessage/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, Message, AudioFileID, Interval, Duration, DisplayLine, DisplayColors, Start, Reoccurring, IntervalQty")] AutoMessage automessage, HttpPostedFileBase uploadFile)
        {
            var amToUpdate = _repo.GetById<AutoMessage>(automessage.Id);

            if (ModelState.IsValid)
            {
                var tzi = (TimeZoneInfo)Session["tzi"];
                if (uploadFile != null)
                {
                    automessage.Image = new byte[uploadFile.ContentLength];
                    uploadFile.InputStream.Read(automessage.Image, 0, uploadFile.ContentLength);
                    amToUpdate.Image = automessage.Image;
                }
                amToUpdate.Message = automessage.Message;
                amToUpdate.AudioFileID = automessage.AudioFileID;
                amToUpdate.Interval = automessage.Interval;
                amToUpdate.IntervalQty = automessage.IntervalQty;
                amToUpdate.Duration = automessage.Duration;
                amToUpdate.DisplayLine = automessage.DisplayLine;
                amToUpdate.DisplayColors = automessage.DisplayColors;

                amToUpdate.Start = timeconversions.ClientToUTC(tzi, automessage.Start);
                amToUpdate.Reoccurring = automessage.Reoccurring;
                var name = User.Identity.Name;
                var user = _repo.GetFirst<Employee>(x => x.LogIn.Trim() == name.Trim());
                if (user != null)
                    amToUpdate.EmployeeID = user.Id;

                _repo.Update(amToUpdate, User.Identity.Name);
                _repo.Save();
                return RedirectToAction("Index");
            }
            PopulateAudioFileDD(automessage.AudioFileID);
            return View(automessage);
        }

        //
        // GET: /AutoMessage/Delete/5

        public ActionResult Delete(int id = 0)
        {
            var automessage = _repo.GetById<AutoMessage>(id);
            if (automessage == null)
            {
                return HttpNotFound();
            }
            return View(automessage);
        }

        //
        // POST: /AutoMessage/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var automessage = _repo.GetById<AutoMessage>(id);
            _repo.Delete(automessage);
            _repo.Save();
            return RedirectToAction("Index");
        }

        public ActionResult GetDateTime()
        {
            var tzi = (TimeZoneInfo) Session["tzi"];
            if (tzi == null)
                return PartialView("_DateTime");
            var now = timeconversions.UTCtoClient(tzi, DateTime.UtcNow);

            ViewBag.Date = now.ToShortDateString();
            ViewBag.Time = now.Hour.ToString().PadLeft(2, '0') + now.Minute.ToString().PadLeft(2, '0');
            return PartialView("_DateTime");
        }

        [HttpPost]
        public ActionResult SetDateTime(string date, string time)
        {
            var startdate = DateTime.Parse(date);
            var hour = double.Parse(time.Substring(0, 2));
            var min = double.Parse(time.Substring(2, 2));
            startdate = startdate.AddHours(hour);
            startdate = startdate.AddMinutes(min);
            return Json(startdate.ToString("M/dd/yyyy HH:mm:ss"), JsonRequestBehavior.AllowGet);
        }

    }
}