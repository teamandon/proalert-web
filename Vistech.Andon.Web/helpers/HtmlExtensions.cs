﻿using System;
using System.Web.Mvc;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;


namespace Vistech.Andon.Web.helpers
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString Image(this HtmlHelper html, byte[] image)
        {
            if (image == null) return new MvcHtmlString("<img src='about:blank' id='previewimg' alt=''>");
            var img = $"data:image/jpg;base64,{Convert.ToBase64String(image)}";
            return new MvcHtmlString("<img id='previewimg' src='" + img + "' />");
        }

        public static string ImageString(this HtmlHelper html, byte[] image)
        {
            if (image == null) return "";
            var imageBase64 = Convert.ToBase64String(image);
            var imageSrc = $"data:image/gif;base64,{imageBase64}";
            return imageSrc;
        }
        public static MvcHtmlString AntiForgeryTokenForAjaxPost(this HtmlHelper helper)
        {
            var antiForgeryInputTag = helper.AntiForgeryToken().ToString();
            // Above gets the following: <input name="__RequestVerificationToken" type="hidden" value="PnQE7R0MIBBAzC7SqtVvwrJpGbRvPgzWHo5dSyoSaZoabRjf9pCyzjujYBU_qKDJmwIOiPRDwBV1TNVdXFVgzAvN9_l2yt9-nf4Owif0qIDz7WRAmydVPIm6_pmJAI--wvvFQO7g0VvoFArFtAR2v6Ch1wmXCZ89v0-lNOGZLZc1" />
            var removedStart = antiForgeryInputTag.Replace(@"<input name=""__RequestVerificationToken"" type=""hidden"" value=""", "");
            var tokenValue = removedStart.Replace(@""" />", "");
            if (antiForgeryInputTag == removedStart || removedStart == tokenValue)
                throw new InvalidOperationException("Oops! The Html.AntiForgeryToken() method seems to return something I did not expect.");
            return new MvcHtmlString(string.Format(@"{0}:""{1}""", "__RequestVerificationToken", tokenValue));
        }

        public static MvcHtmlString UtcToClient(this HtmlHelper html, DateTime? ts, string name, IRepository repo)
        {
            var employee = repo.GetFirst<Employee>(x => x.LogIn == name);
            if (employee == null || ts == null) return null;
            return new MvcHtmlString(employee.GetUserTime(ts).ToShortDateString());

        }

        //public static MvcHtmlString UtcToClient(this HtmlHelper html, DateTime? ts)
        //{
        //    var name = WebSecurity.CurrentUserName;
        //    using (var db = new ProAndonContext())
        //    {
        //        var employee = db.Employees.First(x => x.LogIn.Trim() == name);
        //        if (employee == null || ts == null) return null;
        //        return employee.GetUserTime(ts).ToString();
        //    }
        //}
    }
}