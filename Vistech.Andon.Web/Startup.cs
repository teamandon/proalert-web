﻿using System.Web.Configuration;
using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(Vistech.Andon.Web.Startup))]
namespace Vistech.Andon.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
