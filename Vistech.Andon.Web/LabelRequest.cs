﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web
{
    public class LabelRequest : ILabelRequest
    {
        private volatile bool _sendingShippingRequest;
        private volatile bool _sendingScrapRequest;
        private readonly object _sendShippingRequest = new object();
        private readonly object _sendScrapRequest = new object();

        private ConnectionMapping<string> _connections;
        private IHubConnectionContext<dynamic> Clients { get; set; }

        public ConnectionMapping<string> Connections
        {
            set { _connections = value; }
        }
        public LabelRequest(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
        }

        public Task SendScrapLabelRequest(int scrapId)
        {
            lock (_sendScrapRequest)
            {
                if (!_sendingScrapRequest)
                {
                    SendScrapRequest(scrapId);
                }
                
            }
            return null;
        }
        private void SendScrapRequest(int scrapId)
        {
            Clients.All.printScrapLabel(scrapId);
        }

        public Task SendShippingLabelRequest(ShippingLabelRequest slr)
        {
            lock (_sendShippingRequest)
            {
                if (!_sendingShippingRequest)
                {
                    SendShippingRequest(slr);
                }
            }
            return null;
        }

        private void SendShippingRequest(ShippingLabelRequest slr)
        {
            Clients.All.printShippingLabel(slr);
        }
    }
}