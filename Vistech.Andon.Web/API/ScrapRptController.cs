﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web.API
{
    public class ScrapRptController : ApiController
    {
        private readonly IRepository _repo;

        public ScrapRptController(IRepository repo)
        {
            _repo = repo;
        }

        public HttpResponseMessage Get()
        {
            var tsids = _repo.GetAll<Scrap>().Select(x => x.TimesegmentID.ToString()).ToList();
            var data = _repo.GetScrapRpts(tsids);

            data.Repository = _repo;

            var tbl = data.ConvertToDataTableWithTZ(data.Get());
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            var header = tbl.Columns.Cast<DataColumn>().Aggregate(string.Empty, (current, dc) => current + (dc.ColumnName + ","));

            writer.WriteLine(header.Substring(0, header.Length - 1));
            var d = string.Empty;
            foreach (DataRow dr in tbl.Rows)
            {
                d = tbl.Columns.Cast<DataColumn>().Aggregate(d, (current, dc) => current + (dr[dc.ColumnName] + ","));
                d = d.Substring(0, d.Length - 1);
                writer.WriteLine(d);
                d = string.Empty;
            }

            writer.Flush();
            stream.Position = 0;

            var result = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StreamContent(stream) };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "Scrap.csv" };
            return result;
        }
    }
}