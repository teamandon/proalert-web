﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNet.SignalR.Hubs;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;
using ProAlert.Andon.Service.ViewModels;
using Vistech.Andon.Web.Tools;

namespace Vistech.Andon.Web
{
    public class WorkCenterOee : IWorkCenterOee
    {
        private readonly Timer _timer;
        private volatile bool _updatingWorkCenterOee;
        private readonly object _updateWorkCenterOeeLock = new object();
        private ConnectionMapping<string> _connections;

        public ConnectionMapping<string> Connections
        {
            set { _connections = value; }
        }
        public WorkCenterOee(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;

            _timer = new Timer { AutoReset = false };
            _timer.Elapsed += (TimeElapsed);
            _timer.Start();
        }

        private IHubConnectionContext<dynamic> Clients { get; set; }

        static double GetInterval()
        {
            var now = DateTime.Now;
            return ((60 - now.Second) * 1000 - now.Millisecond) % 6;
        }
        private void TimeElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var now = DateTime.Now;
            // check every ten seconds
            if (!_updatingWorkCenterOee)
                UpdateWcGui();
            _timer.Interval = 10000; //GetInterval();
            _timer.Start();
        }

        private void UpdateWcGui()
        {
            lock (_updateWorkCenterOeeLock)
            {
                if (!_updatingWorkCenterOee)
                {
                    try
                    {
                        _updatingWorkCenterOee = true;
                        using (var context = new ProAlertContext())
                        {
                            //context.Configuration.ProxyCreationEnabled = false;
                            using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                            {
                                //  Manage GuiRunning 
                                //  WorkCenterController will set to True when selected.  
                                // Then we'll let signalr hub 'notify' us that the workcenter gui was closed.
                                foreach (var wc in repo.Get<WorkCenter>())
                                {
                                    wc.GuiRunning = _connections.GetConnections("WorkCenter" + wc.Id).Any();
                                }
                                repo.Save();
                                foreach (var wc in repo.Get<WorkCenter>(x => x.GuiRunning))
                                {
                                    var tempTlVm = new ProductTimelineStatsVM(repo.GetOne<WCProductTimeline>(x => x.WorkCenterID == wc.Id && x.Current, "ShiftLog, ProductWCStatsHistory, Timesegments, WorkCenter, Product"), repo)
                                    {
                                        User = "WcOeeHub"
                                    };
                                    tempTlVm.repo.Save();
                                    var obj = new TlRpt();
                                    var tl = obj.GetTlByWc(wc.Id, wc.OEEStartTime, DateTime.UtcNow);
                                    tl.WorkCenterID = wc.Id;
                                    BroadcastWcUpdate(tl);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        using (var context = new ProAlertContext())
                        {
                            using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                            {
                                repo.InsertError(new AppError
                                {
                                    ErrorDT = DateTime.UtcNow,
                                    ErrorMsg = ex.Message,
                                    Source = "WorkCenterOee"
                                });
                            }
                        }
                    }
                    finally
                    {
                        _updatingWorkCenterOee = false;

                    }
                }
            }

        }
        public Task UpdateWcGui(int wcid)
        {
            lock (_updateWorkCenterOeeLock)
            {
                if (!_updatingWorkCenterOee)
                {
                    try
                    {
                        _updatingWorkCenterOee = true;
                        using (var context = new ProAlertContext())
                        {
                            using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                            {
                                var wc = repo.GetById<WorkCenter>(wcid);
                                var obj = new TlRpt();
                                var tl = obj.GetTlByWc(wc.Id, wc.OEEStartTime, DateTime.UtcNow);
                                BroadcastWcUpdate(tl);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        using (var context = new ProAlertContext())
                        {
                            using (var repo = new EntityFrameworkRepository<ProAlertContext>(context))
                            {
                                repo.InsertError(new AppError
                                {
                                    ErrorDT = DateTime.UtcNow,
                                    ErrorMsg = ex.Message,
                                    Source = "WorkCenterOee"
                                });
                            }
                        }
                    }
                    finally
                    {
                        _updatingWorkCenterOee = false;

                    }
                }
            }
            return null;
        }

        private void BroadcastWcUpdate(ProductTimelineStatsVM vm)
        {
            var wcLogin = "WorkCenter" + vm.WorkCenterID;
            var connIds = _connections.GetConnections(wcLogin);
            foreach (var connId in connIds)
            {
                Clients.Client(connId).updateWcStats(vm.StatsWc);
                Clients.Client(connId).updateOeeHeader(vm.OeeHeaderWc);
                if (vm.TL.ProductID == null)
                    Clients.Client(connId).updateWcSystemMsg("Please select a product.");
                else
                {
                    Clients.Client(connId).updateWcSystemMsg("");
                }
            }
        }

        private void BroadcastUpdate(OEESummaryDetail sd)
        {
            Clients.All.Update(sd);
        }

        private void BroadcastCallList(List<ActiveCall> list)
        {
            var wcLogin = "WorkCenter" + list[0].WorkCenterId;
            var connIds = _connections.GetConnections(wcLogin);
            foreach (var connId in connIds)
            {
                //Clients.Client(connId).updateActiveCalls(list);
            }
        }
    }
}