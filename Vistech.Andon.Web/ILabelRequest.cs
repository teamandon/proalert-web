﻿using System.Threading.Tasks;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web
{
    public interface ILabelRequest
    {
        ConnectionMapping<string> Connections { set; }
        Task SendScrapLabelRequest(int scrapId);
        Task SendShippingLabelRequest(ShippingLabelRequest slr);
    }
}