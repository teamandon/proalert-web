using System.Threading.Tasks;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web
{
    public interface IWorkCenterOee
    {
        ConnectionMapping<string> Connections { set; }
        Task UpdateWcGui(int wcid);
    }
}