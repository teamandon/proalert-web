﻿using System.Threading.Tasks;
using ProAlert.Andon.Service.Interfaces;
using ProAlert.Andon.Service.Models;

namespace Vistech.Andon.Web
{
    public interface IAndonDisplay
    {
        AndonDisplayMsgSet GetMsgSet();
        AdvisoryMsgSet GetAdSet();
        Task InitiateCall(ProAlertContext context, int id);
        void CustomMsg(string advisory);
    }
}