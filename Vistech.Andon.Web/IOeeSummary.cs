using System;
using System.Threading.Tasks;

namespace Vistech.Andon.Web
{
    public interface IOeeSummary
    {
        //DateTime Start { get; set; }
        Task UpdateAll();
        Task InitiateUpdate(int id);
    }
}